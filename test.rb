# just test program
require './PrimeNumberGenerator'

def what_to_do(arguments)
	prime = PrimeNumberGenerator.new
	case arguments.shift
		when "gen", "generate"
			print "#{prime.generate arguments.shift.to_i, arguments.shift.to_i}\n"
		when "check", "ch"
			puts prime.isPrime arguments.shift.to_i
	end
end

case ARGV.shift
	when "-i"
		puts "interactive mode. enter 'q' to exit"
		puts "commands are 'gen(erate) x y', 'ch(eck) x'"
		loop do
			print " > "
			@command = STDIN.gets.split(" ")
			if @command[0] == "q"; puts "exit"; break end
			
			what_to_do @command
		end

	when "-c"
		what_to_do ARGV

	else
		puts "-i for interactive"
		puts "-c for command:"
		puts "\tgen(erate) x y -> to generate sequence from x to y"
		puts "\tch(eck) x -> to check if x is prime"
		puts "HINT: empty = zero ;-)"
end
