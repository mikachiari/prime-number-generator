require '../PrimeNumberGenerator'
require 'prime'

describe PrimeNumberGenerator do
    it 'should exist :-)' do
        PrimeNumberGenerator.new
    end

    context "generator" do
        it 'should return array' do
            gen = PrimeNumberGenerator.new
            expect(gen.generate(1, 555)).to be_an_instance_of(Array)
        end

        it 'should return empty array when input values < 0' do
            gen = PrimeNumberGenerator.new
            expect(gen.generate(-4, -15).empty?).to be true
            expect(gen.generate(-123, -1).empty?).to be true
        end

        it 'should return good values (will test from 13 to 999)' do
            gen = PrimeNumberGenerator.new
            # let's generate prime numbers from 13 to 999
            t = Prime.entries 999
            expect(gen.generate(13, 999)).to match_array(t.drop_while do |x| x<13 end)
        end

        it 'should return good values with any order of the incoming values' do
            gen = PrimeNumberGenerator.new
            first_min = gen.generate(10, 101)
            first_max = gen.generate(101, 10)
            expect(first_max).to match_array(first_min)
        end

        it 'should return endpoints (generate from 10 to 100)' do
            gen = PrimeNumberGenerator.new
            tempo = gen.generate( 11, 97)
            expect(tempo.first).to  eq(11)
            expect(tempo.last).to   eq(97)
        end

        it 'should return good values in range 7900-7920' do
            gen = PrimeNumberGenerator.new
            expect(gen.generate(7900, 7920)).to     match_array([7901,7907, 7919])
        end
    end

    context "checker" do
        it 'should return true or false' do
            ch = PrimeNumberGenerator.new
            expect([true, false]).to           include(ch.isPrime(-1))
        end

        it 'should give good answers on random values' do
            ch = PrimeNumberGenerator.new
            10.times do
                t = Random.rand(10000)
                expect(ch.isPrime t).to         be Prime.prime? t
            end
        end

        it 'should give good answers in range 7900-7920' do
            ch = PrimeNumberGenerator.new
            (7900..7920).each do |x|
                expect(ch.isPrime x).to         be Prime.prime? x
            end
        end

        it 'should answer false when value < 0' do
            ch = PrimeNumberGenerator.new
            expect(ch.isPrime(-10)).to          be false
            expect(ch.isPrime(-1)).to           be false
            expect(ch.isPrime(-110)).to         be false
        end
    end

end
