class PrimeNumberGenerator
	def method_missing(sym, *args, &b)
		"Karrrambaaa!"
	end

	def generate(startingValue, endingValue)
		if (startingValue < 0 && endingValue < 0) || ([startingValue, endingValue].max < 2)
			return []
		end

		# no comments :-)
		@fst = ([startingValue, endingValue].min < 0)? 0 : [startingValue, endingValue].min
		@lst = [startingValue, endingValue].max
		# init values
		@primes = [2]

		# search from 3 to last. then kill first unneeded elements
		# this is not best algorithm, but it works ^_^
		(3..@lst).step 2 do |pretendent|
			@primes.each do |prime|
				if (prime**2) -1 >= pretendent # found new member
					@primes.push pretendent
					break
				end

				if (pretendent % prime) == 0 # is not prime :(
					break
				end
			end
		end
		@primes.drop_while do |x| x<@fst end # drop unneeded elements
	end

	def isPrime(value)
		if value == 2; return true end
		if value < 2; return false end
		self.generate(2, value).last == value
	end
end
